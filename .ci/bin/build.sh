#!/bin/bash

set -eu

build_destination=$(pwd)/public

cd src

# For each directory with a .version file, get the path of the directory with leading "./" stripped, and iterate
for module_path in $(find . -name .version | sed 's/\/.version$//' | sed 's/^.\///') ; do
  version=$(cat $module_path/.version)
  dest_path="$build_destination/$module_path"

  mkdir -p $dest_path
  zip -rj "$dest_path/$version.zip" $module_path/*
done
