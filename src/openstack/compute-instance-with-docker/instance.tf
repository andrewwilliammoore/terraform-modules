data "openstack_images_image_v2" "ubuntu" {
  name        = "Ubuntu 23.04"
  most_recent = true
}

resource "openstack_blockstorage_volume_v3" "instance" {
  # Versioning is used due to the resource and its instance not updating when changing image_id
  name        = "${var.app_name}-instance-volume-1-v1.1.0"
  description = "Storage for ${var.app_name}"
  size        = 20

  image_id = data.openstack_images_image_v2.ubuntu.id
}

resource "openstack_compute_instance_v2" "instance" {
  name            = "${var.app_name}-instance-v1"
  flavor_name     = var.instance_flavor_name
  security_groups = [openstack_networking_secgroup_v2.instance.name]

  user_data = templatefile(
    "${path.module}/cloud-init.yaml",
    { ssh_public_key : openstack_compute_keypair_v2.instance.public_key }
  )

  block_device {
    uuid                  = openstack_blockstorage_volume_v3.instance.id
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    name = var.network_name
  }
}

resource "openstack_compute_keypair_v2" "instance" {
  name       = var.app_name
  public_key = var.instance_public_ssh_key
}