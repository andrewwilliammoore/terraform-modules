variable "app_name" {
  description = "The name to be used in creating resources"
  type        = string
}

variable "instance_public_ssh_key" {
  description = "The public ssh key to be used to access the instance."
  type        = string
}

variable "instance_flavor_name" {
  description = "The type and size of the instance"
  default     = "d2-2" # specific to OVH provider. override with your own
  type        = string
}

variable "network_name" {
  type = string
}

variable "additional_ports" {
  description = "Extra ports needed for the app to run (ex. 80, 443, etc). 22 is open for ssh."
  default     = []

  type = list(object({
    protocol = string
    port     = number
  }))
}
