# Computing Instance with Running Docker Host

Easily deploy containers on single instance. Generate an ssh key for access. Includes a floating IP address connected to the instance.

## Usage

```hcl
module "my_compute_instance" {
  source = "https://andrewwilliammoore.gitlab.io/terraform-modules/openstack/compute-instance-with-docker/2.0.1.zip"

  providers = {
    openstack = openstack
  }

  app_name                = "my-cool-app"
  instance_public_ssh_key = "abc123" # public key resulting from ssh-keygen
  instance_flavor_name    = "t2.micro" # The available instance flavors are set by your cloud provider
  network_name            = "Ext-Net" # Depends on your cloud provider and what you've configured

  # Optional. Port 22 is already open for ssh, egress is open
  additional_ports = [
    {
      protocol = "tcp"
      port     = 80
    },
    {
      protocol = "tcp"
      port     = 443
    },
  ]
}
```

When the infrastructure is applied, you can use regular Docker commands to control the instance by setting the following environment variable in your shell:
```bash
export DOCKER_HOST="ssh://deployer@$(terraform output floating_ip | tr -d \")"
```
