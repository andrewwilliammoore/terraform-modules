output "floating_ip" {
  # TODO: Use actual floating ip, not instance ip
  # TODO: use ip v6, currently fails when assigning to dns, maybe depending on dns provider?
  value = openstack_compute_instance_v2.instance.network[0].fixed_ip_v4
}
