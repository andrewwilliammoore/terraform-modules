resource "openstack_networking_secgroup_v2" "instance" {
  name                 = "${var.app_name}-instance"
  description          = "Security group for accessing the instance"
  delete_default_rules = true
}

resource "openstack_networking_secgroup_rule_v2" "instance_egress_ipv4" {
  direction         = "egress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.instance.id
}

resource "openstack_networking_secgroup_rule_v2" "instance_egress_ipv6" {
  direction         = "egress"
  ethertype         = "IPv6"
  security_group_id = openstack_networking_secgroup_v2.instance.id
}

resource "openstack_networking_secgroup_rule_v2" "instance_ssh_access_ipv4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.instance.id
}

resource "openstack_networking_secgroup_rule_v2" "instance_ssh_access_ipv6" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "::/0"
  security_group_id = openstack_networking_secgroup_v2.instance.id
}

resource "openstack_networking_secgroup_rule_v2" "instance_additional_ports_ipv4" {
  for_each = { for i, v in var.additional_ports : i => v }

  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = each.value.protocol
  port_range_min    = each.value.port
  port_range_max    = each.value.port
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.instance.id
}

resource "openstack_networking_secgroup_rule_v2" "instance_additional_ports_ipv6" {
  for_each = { for i, v in var.additional_ports : i => v }

  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = each.value.protocol
  port_range_min    = each.value.port
  port_range_max    = each.value.port
  remote_ip_prefix  = "::/0"
  security_group_id = openstack_networking_secgroup_v2.instance.id
}
