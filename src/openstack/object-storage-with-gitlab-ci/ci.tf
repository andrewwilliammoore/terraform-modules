data "gitlab_project" "this" {
  id = var.gitlab_project_id
}

resource "gitlab_project_variable" "storage_container_name" {
  project = data.gitlab_project.this.id
  key     = "STORAGE_CONTAINER_NAME"
  value   = openstack_objectstorage_container_v1.this.name
}

resource "gitlab_project_variable" "storage_project_id" {
  project   = data.gitlab_project.this.id
  key       = "STORAGE_PROJECT_ID"
  value     = openstack_identity_ec2_credential_v3.build_uploader.project_id
  masked    = true
  protected = true
}

resource "gitlab_project_variable" "storage_access_key_id" {
  project   = data.gitlab_project.this.id
  key       = "STORAGE_ACCESS_KEY_ID"
  value     = openstack_identity_ec2_credential_v3.build_uploader.access
  masked    = true
  protected = true
}

resource "gitlab_project_variable" "storage_access_key_secret" {
  project   = data.gitlab_project.this.id
  key       = "STORAGE_ACCESS_KEY_SECRET"
  value     = openstack_identity_ec2_credential_v3.build_uploader.secret
  masked    = true
  protected = true
}
