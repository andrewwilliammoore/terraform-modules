output "storage_container" {
  value = openstack_objectstorage_container_v1.this
}

output "project_id" {
  value = openstack_identity_ec2_credential_v3.build_uploader.project_id
}