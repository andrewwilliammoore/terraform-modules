terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.49.0"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.4.0"
    }
  }
}
