# Object Storage with Gitlab CI

Creates on OpenStack object storage container with public read access and credentialed write access. Provides the credentials as masked project variables for CI for an existing Gitlab repo. These values will only be available on protected branches to prevent submitted feature branches from introducing malicious scripts for stealing the credentials.

## Usage

```hcl
module "object_storage_with_ci_pipeline" {
  source = "https://andrewwilliammoore.gitlab.io/terraform-modules/openstack/object-storage-with-gitlab-ci/2.0.0.zip"

  providers = {
    openstack = openstack
    gitlab    = gitlab
  }

  object_storage_container_name = "my-build-upload-destination"
  gitlab_project_id             = "abc123" # Seen on your repo's public-facing UI
  container_read                = ".r:placeholder_referer" # For example to only allow read access for a specified referer
  region                        = "DE" # This will depend on your cloud provider's available storage regions
}
```

Then, to use this in the CI pipeline, the following serves as an example. Get the `HOST_FOR_OPENSTACK_PROVIDER_OBJECT_STORAGE` value from your specific OpenStack provider.
```yml
upload:
  script:
    - s3cmd \
        --access_key "$STORAGE_ACCESS_KEY_ID" \
        --secret_key "$STORAGE_ACCESS_KEY_SECRET" \
        --host HOST_FOR_OPENSTACK_PROVIDER_OBJECT_STORAGE \
        --host-bucket HOST_FOR_OPENSTACK_PROVIDER_OBJECT_STORAGE/$STORAGE_PROJECT_ID/$STORAGE_CONTAINER_NAME \
        --exclude ".git/*" \
        put -r  ./ s3://$STORAGE_CONTAINER_NAME
```
