# TODO: Try changing to specific user, rather than using credential generated for terraform user
resource "openstack_identity_ec2_credential_v3" "build_uploader" {}

# For restricting access to storage container
resource "random_id" "permitted_referer_id" {
  byte_length = 64
}

resource "openstack_objectstorage_container_v1" "this" {
  name            = var.object_storage_container_name
  container_write = "${openstack_identity_ec2_credential_v3.build_uploader.project_id}:${openstack_identity_ec2_credential_v3.build_uploader.user_id}"
  # Use the HTTP Referer header for authorization. Using projec_id:user_id combination requires creating an auth token
  #   via the cli and using that value in any requests.
  #
  # According to docs, it's also possible to use a role here.
  # https://docs.openstack.org/swift/latest/overview_acl.html#example-sharing-a-container-with-users-having-a-specified-role
  container_read = var.container_read

  region = var.region
}
