variable "gitlab_project_id" {
  description = "The project id of your repo. This is found on the public UI of your repo."
  type        = number
}

variable "object_storage_container_name" {
  description = "The name of the object storage container to be created."
  type        = string
}

variable "container_read" {
  description = "The read ACL policy for the object storage container. See openstack swift docs for info"
  type        = string
}

variable "region" {
  description = "The region for your object storage container. Note that this is specific to your cloud provider"
  type        = string
}