resource "random_uuid" "object_storage_permitted_referer" {}

module "object_storage_with_ci_pipeline" {
  source = "https://andrewwilliammoore.gitlab.io/terraform-modules/openstack/object-storage-with-gitlab-ci/2.0.0.zip"

  providers = {
    openstack = openstack
    gitlab    = gitlab
  }

  object_storage_container_name = var.app_name
  gitlab_project_id             = var.gitlab_project_id
  container_read                = ".r:${random_uuid.object_storage_permitted_referer.result}"
  region                        = var.object_storage_region
}
