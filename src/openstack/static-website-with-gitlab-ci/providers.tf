terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.49.0"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.4.0"
    }

    acme = {
      source  = "vancluever/acme"
      version = "~> 2.8.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "~> 2.2.2"
    }
  }
}
