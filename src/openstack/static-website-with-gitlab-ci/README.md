# Static Website with Gitlab CI

Creates infrastructure for a basic static website, served via HTTPS. Does not use a CDN but instead a simple compute instance with an nginx reverse proxy. Adds needed environment variables for uploading build artifacts to a Gitlab CI pipeline.

Resources:
- object storage
- compute instance for https proxy

Accounts Needed:
- Openstack
- Gitlab

Host Machine Dependencies:
- Docker

## Usage
Sample usage:
```terraform
module "static_website" {
  source = "https://andrewwilliammoore.gitlab.io/terraform-modules/openstack/static-website-with-gitlab-ci/1.0.1.zip"

  providers = {
    openstack = openstack
    gitlab    = gitlab
    acme      = acme
    local     = local
  }

  gitlab_project_id       = 123
  fqdn                    = "www.my-site.com"
  app_name                = "my-site"
  proxy_public_ssh_key    = "public-ssh-key-value" # Create an ssh key that will be used for the proxy server
  provider_storage_domain = "object.api.ams2.fuga.cloud" # For example
  public_network_name     = "Ext-Net" # Depends on cloud provider
  object_storage_region   = "DE" # Depends on cloud provider
  acme_registration_email = "admin@my-site.com"
  dns_challenge_provider  = "digitalocean" # For example

  dns_challenge_config = {
    DO_AUTH_TOKEN = var.DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN # For example
  }
}
```
See `variables.tf` for more information on inputs.

You will need some configuration for providers when including this module:

```terraform
provider "openstack" {}

provider "acme" {
  # server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}
```

## Troubleshooting
The proxy server takes some time to start and provision, so you may encounter errors when the script attempts to run the nginx docker image on the server. In this case, it is sufficient to wait a while and try applying the terraform config again. This can hopefully be done programmatically in an upcoming version.
