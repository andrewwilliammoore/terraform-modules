resource "tls_private_key" "this" {
  algorithm = "RSA"
}

resource "acme_registration" "this" {
  account_key_pem = tls_private_key.this.private_key_pem
  email_address   = var.acme_registration_email
}

resource "acme_certificate" "this" {
  account_key_pem = acme_registration.this.account_key_pem
  common_name     = var.fqdn

  dns_challenge {
    provider = var.dns_challenge_provider
    config   = var.dns_challenge_config
  }
}
