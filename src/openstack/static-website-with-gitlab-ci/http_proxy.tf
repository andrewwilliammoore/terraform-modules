module "http_proxy" {
  source = "https://andrewwilliammoore.gitlab.io/terraform-modules/openstack/compute-instance-with-docker/2.0.1.zip"

  providers = {
    openstack = openstack
  }

  app_name                = var.app_name
  instance_public_ssh_key = var.proxy_public_ssh_key # TODO: consider creating ssh key within terraform, would make troubleshooting harder, though
  network_name            = var.public_network_name

  additional_ports = [
    {
      protocol = "tcp"
      port     = 80
    },
    {
      protocol = "tcp"
      port     = 443
    },
  ]
}

resource "local_sensitive_file" "http_proxy_nginx_conf" {
  # Filename is specific in order to overwrite default server definitions from docker image
  filename = "${path.module}/conf.d/default.conf"

  content = templatefile("${path.module}/default.conf.template", {
    # AUTH_ is just project ID, not sensitive
    backend_origin = "https://storage.de.cloud.ovh.net/v1/AUTH_79825fd91b2c402ea52e8ad7a1d1ff06/portfolio"
    referer        = random_uuid.object_storage_permitted_referer.result
  })

  file_permission = "700"
}

resource "local_sensitive_file" "http_proxy_tls_cert" {
  filename = "${path.module}/conf.d/cert.crt"
  content  = "${acme_certificate.this.certificate_pem}${acme_certificate.this.issuer_pem}"

  file_permission = "700"
}

resource "local_sensitive_file" "http_proxy_tls_cert_key" {
  filename = "${path.module}/conf.d/cert.key"
  content  = acme_certificate.this.private_key_pem

  file_permission = "700"
}

resource "null_resource" "http_proxy_add_ip_to_hosts" {
  provisioner "local-exec" {
    # TODO: try ssh-keygen -f "/home/drew/.ssh/known_hosts" -R FLOATING_IP
    command = "ssh-keyscan ${module.http_proxy.floating_ip} >> ~/.ssh/known_hosts"
  }
}

# TODO: wait for server to be ready before trying to provision. An error occurs otherwise.
# TODO: need to add instance ip to host machine's known_hosts programmatically. Currently errors and then need to manually ssh first time
resource "null_resource" "http_proxy_provisioner" {
  provisioner "local-exec" {
    command = "${path.module}/proxy_deployer.sh"

    environment = {
      DOCKER_HOST     = "ssh://deployer@${module.http_proxy.floating_ip}"
      NGINX_CONF_PATH = "${path.module}/conf.d"
    }
  }

  depends_on = [
    local_sensitive_file.http_proxy_nginx_conf,
    local_sensitive_file.http_proxy_tls_cert,
    local_sensitive_file.http_proxy_tls_cert_key
  ]

  triggers = {
    file_ids = "${local_sensitive_file.http_proxy_nginx_conf.id}:${local_sensitive_file.http_proxy_tls_cert.id}:${local_sensitive_file.http_proxy_tls_cert_key.id}"
  }
}
