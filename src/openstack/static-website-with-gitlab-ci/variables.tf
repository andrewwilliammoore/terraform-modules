# The project ID of the Gitlab project that builds the website in its CI pipeline
# TODO: try to make flexible with provider
variable "gitlab_project_id" {}

# The fully-qualified domain name of your static site, ex: www.mysite.com
variable "fqdn" {}

# Name of your app or service
variable "app_name" {}

# The public key of an ssh key pair that you generate
# TODO: allow creating this purely internally. Makes it harder to debug, though due to not having access to proxy server
variable "proxy_public_ssh_key" {}

# The domain that your openstack provider uses for object storage, ex: object.api.ams2.fuga.cloud
variable "provider_storage_domain" {}

variable "public_network_name" {
  description = "the name of the public network created by your cloud provider"
  type        = string
}

variable "object_storage_region" {
  description = "The region for your website's object storage container. Note that this is specific to your cloud provider"
  type        = string
}

# Email address to use for registering your TLS certificate
variable "acme_registration_email" {}

# Name of provider to use for DNS challenge to verify TLS certificate, ex: digitalocean
# See list of available providers:
# https://community.letsencrypt.org/t/dns-providers-who-easily-integrate-with-lets-encrypt-dns-validation/86438
variable "dns_challenge_provider" {}

# Your provider's specified configuration for the DNS challenge
# Ex: {
#   DO_AUTH_TOKEN = DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN
# }
variable "dns_challenge_config" {}
