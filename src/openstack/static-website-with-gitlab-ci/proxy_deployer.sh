#!/bin/bash

set -eu

stop_existing_container () {
  container_name=$1

  if docker container ls -a | grep $container_name; then
    docker stop $container_name
    docker rm $container_name
  fi
}

nginx_proxy_name=nginx_tls_proxy
stop_existing_container $nginx_proxy_name

# Needed for DNS resolution so that requests to origin can be made.
network_name=proxy_network
if ! docker network ls | grep $network_name; then
  docker network create $network_name
fi

docker run -d \
           --name $nginx_proxy_name \
           --network $network_name \
           -p 80:80 \
           -p 443:443 \
           nginx:1.21.6-alpine

docker cp $NGINX_CONF_PATH $nginx_proxy_name:/etc/nginx
docker restart $nginx_proxy_name
