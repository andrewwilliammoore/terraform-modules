# Terraform Modules
Shared terraform modules

## Structure
Each directory under `./src` that has no children is built to a module. It must have a `.version` file using semantic versioning. The directory structure of the resulting uploaded build follows that of `./src` with each module contained in a zip file with the version as the name.

## Build
```bash
.ci/bin/build.sh
```
